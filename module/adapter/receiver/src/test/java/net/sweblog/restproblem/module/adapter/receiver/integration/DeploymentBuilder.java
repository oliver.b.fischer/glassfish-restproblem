package net.sweblog.restproblem.module.adapter.receiver.integration;

import net.sweblog.restproblem.module.adapter.receiver.MyCollection;
import net.sweblog.restproblem.common.resthelper.JsonMarshallerProvider;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;

public class DeploymentBuilder {

    public WebArchive build()
    {
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                                   .addPackage(JsonMarshallerProvider.class.getPackage())
                                   .addPackage(MyCollection.class.getPackage())
                                   .addAsWebInfResource("WEB-INF/glassfish-web.xml", "glassfish-web.xml")
                                   .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");




        return war;
    }
}
