package net.sweblog.restproblem.module.adapter.receiver.integration;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.mapper.ObjectMapperType;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import net.sweblog.restproblem.common.resthelper.MMMapperBuilder;
import net.sweblog.restproblem.module.adapter.datamodel.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.config.ObjectMapperConfig.objectMapperConfig;
import static org.joda.time.DateTime.now;


@Test
public class CEventsIT
        extends Arquillian
{

    @BeforeClass
    public void configureRestAssured()
    {
        ObjectMapperConfig objectMapperConfig = objectMapperConfig().jackson2ObjectMapperFactory(
                new Jackson2ObjectMapperFactory() {
                    @Override
                    public com.fasterxml.jackson.databind.ObjectMapper create(Class c, String s) {
                        return new MMMapperBuilder().build();
                    }
                });

        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(objectMapperConfig);
    }

    @Deployment
    public static WebArchive create() {
        return new DeploymentBuilder().build();
    }


    public void sendACorrectEvent()
    {
        CEventJTO event = cookForMeACorrectCreateServerEvent();

        given().contentType(ContentType.JSON).body(event, ObjectMapperType.JACKSON_2)
               .expect().statusCode(Response.Status.CREATED.getStatusCode())
               .put("/arquillian/adapter/events/v1/a/c/{id}", event.getPropertyE());


    }

    public void sendAnEventWithHostIdResultsWorks() {
        CEventJTO event = cookForMeACorrectCreateServerEvent();

        event.setPropertyC(UUID.randomUUID());

        given().contentType(ContentType.JSON).body(event, ObjectMapperType.JACKSON_2)
                .expect().statusCode(Response.Status.CREATED.getStatusCode())
                .put("/arquillian/adapter/events/v1/a/c/{id}", event.getPropertyE());
    }

    public void sendAnEventWithoutHostIdWorks() {
        CEventJTO event = cookForMeACorrectCreateServerEvent();

        event.setPropertyC((UUID) TestConstants.I_AM_NULL);

        given().contentType(ContentType.JSON).body(event, ObjectMapperType.JACKSON_2)
                .expect().statusCode(Response.Status.CREATED.getStatusCode())
                .put("/arquillian/adapter/events/v1/a/c/{id}", event.getPropertyE());

    }

    public void sendAnEventWithoutNetworkNameResultsWorks() {
        CEventJTO event = cookForMeACorrectCreateServerEvent();

        event.setPropertyAName((String) TestConstants.I_AM_NULL);

        given().contentType(ContentType.JSON).body(event, ObjectMapperType.JACKSON_2)
                .expect().statusCode(Response.Status.CREATED.getStatusCode())
                .put("/arquillian/adapter/events/v1/a/c/{id}", event.getPropertyE());

    }

    public void sendAnEventWitNetworkNameResultsWorks() {
        CEventJTO event = cookForMeACorrectCreateServerEvent();

        event.setPropertyAName("DN 2");

        given().contentType(ContentType.JSON).body(event, ObjectMapperType.JACKSON_2)
                .expect().statusCode(Response.Status.CREATED.getStatusCode())
                .put("/arquillian/adapter/events/v1/a/c/{id}", event.getPropertyE());

    }

    public void headToNonExistingEventWorks()
    {
        Long id = System.currentTimeMillis();

        expect().statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .head("/arquillian/adapter/events/v1/a/c/{id}", id);
    }

    public void sendAnEventWithoutNetworkId() {
        CEventJTO event = cookForMeACorrectCreateServerEvent();

        event.setNetworkId((UUID) TestConstants.I_AM_NULL);

        given().contentType(ContentType.JSON).body(event, ObjectMapperType.JACKSON_2)
                .expect().statusCode(Response.Status.CREATED.getStatusCode())
                .put("/arquillian/adapter/events/v1/a/c/{id}", event.getPropertyE());
    }

    private CEventJTO cookForMeACorrectCreateServerEvent()
    {
        CEventJTO event = new CEventJTO();

        event.setPropertyE(System.currentTimeMillis());
        event.setPropertyF(new XYZId(System.currentTimeMillis()));
        event.setPropertyK(new Location("place"));
        event.setPropertyJ(UUID.randomUUID());
        event.setCreationType(TypeJTO.TYPE_THAT);
        event.setPropertyG(StateJTO.IN);
        event.setPropertyH(4);
        event.setPropertyI(1024);
        event.setPropertyL(ASystem.KNOWN);
        event.setPropertyD(now());
        event.setNetworkId(UUID.randomUUID());
        event.setPropertyAName("Dsdfsdf" + UUID.randomUUID().toString());

        return event;
    }
}
