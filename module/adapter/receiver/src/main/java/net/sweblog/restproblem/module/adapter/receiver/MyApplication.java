package net.sweblog.restproblem.module.adapter.receiver;

import net.sweblog.restproblem.common.resthelper.JsonMarshallerProvider;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;


@ApplicationPath("/")
public class MyApplication
  extends Application
{
  @Override
  public Set<Class<?>> getClasses()
  {
    return new HashSet<Class<?>>()
    {{
        add(MyCollection.class);
        add(JsonMarshallerProvider.class);
      }};
  }
}
