package net.sweblog.restproblem.module.adapter.receiver;

import net.sweblog.restproblem.module.adapter.datamodel.BaseJTO;
import net.sweblog.restproblem.module.adapter.datamodel.CEventJTO;
import net.sweblog.restproblem.module.adapter.datamodel.DEventJTO;
import net.sweblog.restproblem.module.adapter.datamodel.UEventJTO;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.HEAD;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Locale;
import java.util.ResourceBundle;

@RequestScoped
@Path("adapter/events/v1")
public class MyCollection
{



    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("a/c/{id}")
    public Response storeCreateNicEvent(CEventJTO jto)  {
        return storeEventGeneric(jto);
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("a/u/{id}")
    public Response storeUpdateServerEvent(UEventJTO jto)  {
        return storeEventGeneric(jto);
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("a/d/{id}")
    public Response storeDeleteServerEvent(DEventJTO jto)  {
        return storeEventGeneric(jto);
    }

    @HEAD
    @Path("a/c/{id}")
    public Response checkExistenceOfServerCreateEvent(@PathParam("id") Long id)
    {
        return getResponseForHeadToEvent(id);
    }

    @HEAD
    @Path("a/u/{id}")
    public Response checkExistenceOfServerUpdateEvent(@PathParam("id") Long id)
    {
        return getResponseForHeadToEvent(id);
    }

    @HEAD
    @Path("a/d/{id}")
    public Response checkExistenceOfServerDeleteEvent(@PathParam("id") Long id)
    {
        return getResponseForHeadToEvent(id);
    }





    private Response getResponseForHeadToEvent(Long id) {
        Response response = Response.noContent().status(Response.Status.NOT_FOUND)
                                    .header(HttpHeaders.CONTENT_LENGTH, 0L)
                                    .build();


        return response;
    }


    public Response storeEventGeneric(BaseJTO eventIn)  {

        Response response = buildResponsFor201();

        return response;
    }


    private Response buildResponsFor201() {
        return Response.status(Response.Status.CREATED).entity("Give me more data!").build();
    }
}
