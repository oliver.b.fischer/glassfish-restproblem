package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class XYZId {
    private long id;

    @JsonCreator
    public XYZId(long contractId) {
        id = contractId;
    }


    public long getId() {
        return id;
    }

    public void setId(long contractId) {
        id = contractId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        XYZId contractId = (XYZId) o;

        return new EqualsBuilder()
                .append(id, contractId.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(287, 997).append(id).toHashCode();
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
