package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class XYZIdSerializer extends JsonSerializer<XYZId>
{
    @Override
    public void serialize(XYZId value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException
    {
        jgen.writeNumber(value.getId());
    }
}
