package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class LocationSerializer extends JsonSerializer<Location>
{
    @Override
    public void serialize(Location value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException
    {
        jgen.writeString(value.getName());
    }
}
