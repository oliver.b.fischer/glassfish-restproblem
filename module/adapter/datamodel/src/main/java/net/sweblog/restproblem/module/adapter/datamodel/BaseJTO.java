package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;

import javax.validation.constraints.NotNull;
import java.util.UUID;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
              include = JsonTypeInfo.As.PROPERTY,
              property = "type")
public abstract class BaseJTO {
    @JsonProperty("propertyE")
    private Long propertyE;

    @JsonProperty("propertyD")
    private DateTime propertyD;

    @JsonProperty("propertyF")
    @JsonSerialize(using = XYZIdSerializer.class)
    private XYZId propertyF;

    @JsonProperty("propertyK")
    @JsonSerialize(using = LocationSerializer.class)
    private Location propertyK;

    @JsonProperty("propertyG")
    private StateJTO propertyG;

    @JsonProperty("propertyJ")
    private UUID propertyJ;

    @JsonProperty("creationType")
    private TypeJTO creationType;

    @NotNull()
    public Long getPropertyE() {
        return propertyE;
    }

    public void setPropertyE(Long propertyE) {
        this.propertyE = propertyE;
    }

    @NotNull()
    public DateTime getPropertyD() {
        return propertyD;
    }

    public void setPropertyD(DateTime propertyD) {
        this.propertyD = propertyD;
    }

    @NotNull()
    public UUID getPropertyJ() {
        return propertyJ;
    }

    public void setPropertyJ(UUID uuid) {
        propertyJ = uuid;
    }

    @NotNull()
    public Location getPropertyK() {
        return propertyK;
    }

    public void setPropertyK(Location regionName) {
        propertyK = regionName;
    }

    @NotNull
    public StateJTO getPropertyG() {
        return propertyG;
    }

    public void setPropertyG(StateJTO state) {
        propertyG = state;
    }

    @NotNull()
    public XYZId getPropertyF() {
        return propertyF;
    }

    public void setPropertyF(XYZId contractId) {
        propertyF = contractId;
    }

    @NotNull()
    public TypeJTO getCreationType() {
        return creationType;
    }

    public void setCreationType(TypeJTO creationType) {
        this.creationType = creationType;
    }
}
