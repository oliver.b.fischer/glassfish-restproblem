package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Base2EventJTO
    extends BaseJTO
{
    @JsonProperty("propertyB")
    private UUID networkId;

    @JsonProperty("propertyA")
    private String propertyA;

    @JsonProperty("propertyC")
    private UUID propertyC;


    public UUID getPropertyC() {
        return propertyC;
    }

    public void setPropertyC(UUID id) {
        propertyC = id;
    }

    public String getPropertyAName() {
        return propertyA;
    }

    public void setPropertyAName(String name) {
        propertyA = name;
    }

    public UUID getNetworkId() {
        return networkId;
    }

    public void setNetworkId(UUID id) {
        networkId = id;
    }
}
