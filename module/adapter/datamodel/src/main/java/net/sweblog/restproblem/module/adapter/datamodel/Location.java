package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Location {
    private String name;

    @JsonCreator
    public Location(String nameOfRegion) {
        name = nameOfRegion;
    }


    public String getName() {
        return name;
    }

    public void setName(String regionName) {
        name = regionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Location region = (Location) o;

        return new EqualsBuilder()
                .append(name, region.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(187, 897).append(name).toHashCode();
    }

    @Override
    public String toString() {
        return name;
    }
}
