package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@JsonTypeName("cEvent")
public class CEventJTO
    extends Base2EventJTO
{
    private int propertyH;
    private int propertyI;
    private ASystem propertyL;

    public void setPropertyH(int propertyH) {
        this.propertyH = propertyH;
    }

    @JsonProperty("propertyH")
    @Min(value = 0)
    public int getPropertyH() {
        return propertyH;
    }

    public void setPropertyI(int propertyI) {
        this.propertyI = propertyI;
    }

    @JsonProperty("propertyI")
    @Min(value = 0)
    public int getPropertyI() {
        return propertyI;
    }

    public void setPropertyL(ASystem propertyL) {
        this.propertyL = propertyL;
    }

    @JsonProperty("propertyL")
    @NotNull()
    public ASystem getPropertyL() {
        return propertyL;
    }
}
