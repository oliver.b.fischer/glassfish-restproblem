package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.sweblog.restproblem.common.resthelper.MMMapperBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

@Test
public class DEventJTOTest {
    private ObjectMapper mapper = new MMMapperBuilder().build();

    public void simpleSerialisationWorks() throws JsonProcessingException
    {
        DEventJTO jto = new DEventJTO();

        mapper.writeValueAsString(jto);
    }

    public void serialisationRoundTrip() throws IOException
    {
        UUID resourceUuid = UUID.randomUUID();

        DEventJTO in = new DEventJTO();

        in.setPropertyF(new XYZId(938L));
        in.setPropertyK(new Location("HERE"));
        in.setPropertyJ(resourceUuid);
        in.setPropertyG(StateJTO.OUT);
        in.setPropertyD(new DateTime(2012, 12, 12, 12, 12, 12, 01, DateTimeZone.UTC));
        in.setPropertyC(UUID.fromString("999d6b5a-96c9-11e2-8e51-07bc0e9e2a0b"));

        String json = mapper.writeValueAsString(in);

        DEventJTO out = mapper.readValue(json, DEventJTO.class);

        assertEquals(out.getPropertyK(), new Location("HERE"));
        assertEquals(out.getPropertyF(), new XYZId(938L));
        assertEquals(out.getPropertyJ(), resourceUuid);
        assertEquals(out.getPropertyG(), StateJTO.OUT);
        assertEquals(out.getPropertyD(), new DateTime(2012, 12, 12, 12, 12, 12, 01, DateTimeZone.UTC));
        assertEquals(out.getPropertyC(), UUID.fromString("999d6b5a-96c9-11e2-8e51-07bc0e9e2a0b"));
    }

    public void deserialiseJsonExampleFile() throws IOException
    {
        InputStream is =
                getClass().getClassLoader()
                        .getResourceAsStream("exampleJsonFiles/d-event-ok.json");

        DEventJTO out = mapper.readValue(is, DEventJTO.class);

        assertEquals(out.getPropertyK(), new Location("HERE"));
        assertEquals(out.getPropertyF(), new XYZId(2222L));
        assertEquals(out.getPropertyJ(), UUID.fromString("65ce3144-d51a-4217-bf9c-4804c436fc29"));
        assertEquals(out.getPropertyG(), StateJTO.OUT);
        assertEquals(out.getPropertyD(), new DateTime(2012, 12, 12, 12, 12, 12, 0, DateTimeZone.UTC));
        assertEquals(out.getNetworkId(), UUID.fromString("b74232be-95fa-11e2-881e-c3733469db4c"));
        assertEquals(out.getPropertyC(), UUID.fromString("999d6b5a-96c9-11e2-8e51-07bc0e9e2a0b"));
    }


}
