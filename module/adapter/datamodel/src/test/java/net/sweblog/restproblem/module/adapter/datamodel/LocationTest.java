package net.sweblog.restproblem.module.adapter.datamodel;

import net.sweblog.restproblem.module.adapter.datamodel.Location;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Test
public class LocationTest {

    public void differentRegionsAreNotEqual()
    {
        Location a = new Location("europe/berlin");
        Location b = new Location("south-america/brasil");

        assertNotEquals(a, b);
        assertNotEquals(b, a);
        assertNotSame(a, b);
    }

    public void sameRegionIsEqual()
    {
        Location a = new Location("europe/berlin");
        Location b = new Location("europe/berlin");

        assertEquals(a, b);
        assertEquals(b, a);
        assertNotSame(a, b);
    }

    public void equalsHandlesNullValueProperly()
    {
        Location r = new Location("europe/berlin");

        assertFalse(r.equals(null));
    }

    public void equalsHandlesDifferentTypeProperly()
    {
        Location r = new Location("europe/berlin");

        assertFalse(r.equals("Ich bin ein String!"));
    }

    public void sameRegionNameGivesSameHashValue()
    {
        Location a = new Location("europe/berlin");
        Location b = new Location("europe/berlin");

        assertEquals(a.hashCode(), b.hashCode());
    }

    public void differentRegionNameGivesDifferentHashValue()
    {
        Location a = new Location("europe/berlin");
        Location b = new Location("europe/barcelona");

        assertNotEquals(a.hashCode(), b.hashCode());
    }
}
