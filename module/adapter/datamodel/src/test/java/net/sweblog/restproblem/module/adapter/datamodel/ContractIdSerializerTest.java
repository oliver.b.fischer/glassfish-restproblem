package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.core.JsonGenerator;
import net.sweblog.restproblem.module.adapter.datamodel.Location;
import net.sweblog.restproblem.module.adapter.datamodel.LocationSerializer;
import net.sweblog.restproblem.module.adapter.datamodel.XYZId;
import net.sweblog.restproblem.module.adapter.datamodel.XYZIdSerializer;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@Test
public class ContractIdSerializerTest {

    public void serialisationOfRegionNameWorks() throws IOException {
        XYZId contract = mock(XYZId.class);
        doReturn(123L).when(contract).getId();

        JsonGenerator generator = mock(JsonGenerator.class);

        XYZIdSerializer serializer = new XYZIdSerializer();

        serializer.serialize(contract, generator, null);

        verify(generator).writeNumber(123L);
        verifyNoMoreInteractions(generator);
    }

    @Test
    public static class RegionSerializerTest {

        public void serialisationOfRegionNameWorks() throws IOException {
            Location region = mock(Location.class);
            doReturn("europe/berlin").when(region).getName();

            JsonGenerator generator = mock(JsonGenerator.class);

            LocationSerializer serializer = new LocationSerializer();

            serializer.serialize(region, generator, null);

            verify(generator).writeString("europe/berlin");
            verifyNoMoreInteractions(generator);
        }
    }
}
