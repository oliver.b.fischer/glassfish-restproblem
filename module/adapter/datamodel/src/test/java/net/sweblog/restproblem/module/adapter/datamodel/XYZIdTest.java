package net.sweblog.restproblem.module.adapter.datamodel;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotSame;

@Test
public class XYZIdTest {

    public void differentContracIdsAreNotEqual()
    {
        XYZId a = new XYZId(7383L);
        XYZId b = new XYZId(93874L);

        assertNotEquals(a, b);
        assertNotEquals(b, a);
        assertNotSame(a, b);
    }

    public void sameContractIdIsEqual()
    {
        XYZId a = new XYZId(92823L);
        XYZId b = new XYZId(92823L);

        assertEquals(a, b);
        assertEquals(b, a);
        assertNotSame(a, b);
    }

    public void equalsHandlesNullValueProperly()
    {
        XYZId c = new XYZId(93L);

        assertFalse(c.equals(null));
    }

    public void equalsHandlesDifferentTypeProperly()
    {
        XYZId c = new XYZId(98L);

        assertFalse(c.equals("Ich bin ein String!"));
    }

    public void sameContractIdNameGivesSameHashValue()
    {
        XYZId a = new XYZId(9283L);
        XYZId b = new XYZId(9283L);

        assertEquals(a.hashCode(), b.hashCode());
    }

    public void differentContractIdNameGivesDifferentHashValue()
    {
        XYZId a = new XYZId(9L);
        XYZId b = new XYZId(10L);

        assertNotEquals(a.hashCode(), b.hashCode());
    }
}
