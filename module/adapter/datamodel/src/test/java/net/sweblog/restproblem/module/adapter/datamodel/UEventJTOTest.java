package net.sweblog.restproblem.module.adapter.datamodel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.sweblog.restproblem.common.resthelper.MMMapperBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

@Test
public class UEventJTOTest {
    private ObjectMapper mapper = new MMMapperBuilder().build();

    public void simpleSerialisationWorks() throws JsonProcessingException
    {
        UEventJTO jto = new UEventJTO();

        mapper.writeValueAsString(jto);
    }

    public void serialisationRoundTrip() throws IOException
    {
        UUID resourceUuid = UUID.randomUUID();

        UEventJTO in = new UEventJTO();

        in.setPropertyF(new XYZId(938L));
        in.setPropertyK(new Location("HERE"));
        in.setPropertyJ(resourceUuid);
        in.setPropertyG(StateJTO.OUT);
        in.setPropertyAName("Heiner");
        in.setPropertyH(4);
        in.setPropertyI(96);
        in.setPropertyL(ASystem.UNKNOWN);
        in.setPropertyD(new DateTime(2007, 12, 12, 12, 12, 12, 12, DateTimeZone.UTC));
        in.setPropertyC(UUID.fromString("999d6b5a-96c9-11e2-8e51-07bc0e9e2a0b"));

        String json = mapper.writeValueAsString(in);

        UEventJTO out = mapper.readValue(json, UEventJTO.class);

        assertEquals(out.getPropertyK(), new Location("HERE"));
        assertEquals(out.getPropertyF(), new XYZId(938L));
        assertEquals(out.getPropertyJ(), resourceUuid);
        assertEquals(out.getPropertyG(), StateJTO.OUT);
        assertEquals(out.getPropertyH(), 4);
        assertEquals(out.getPropertyI(), 96);
        assertEquals(out.getPropertyL(), ASystem.UNKNOWN);
        assertEquals(out.getPropertyD(), new DateTime(2007, 12, 12, 12, 12, 12, 12, DateTimeZone.UTC));
        assertEquals(out.getPropertyC(), UUID.fromString("999d6b5a-96c9-11e2-8e51-07bc0e9e2a0b"));
        assertEquals(out.getPropertyAName(), "Heiner");
    }

    public void deserialiseJsonExampleFile() throws IOException
    {
        InputStream is =
                getClass().getClassLoader()
                        .getResourceAsStream("exampleJsonFiles/u-event-ok.json");

        UEventJTO out = mapper.readValue(is, UEventJTO.class);

        assertEquals(out.getPropertyK(), new Location("HERE"));
        assertEquals(out.getPropertyF(), new XYZId(938L));
        assertEquals(out.getPropertyJ(), UUID.fromString("65ce3144-d51a-4217-bf9c-4804c436fc29"));
        assertEquals(out.getPropertyG(), StateJTO.OUT);
        assertEquals(out.getPropertyD(), new DateTime(2012, 12, 12, 12, 12, 12, 12, DateTimeZone.UTC));
        assertEquals(out.getPropertyI(), 96);
        assertEquals(out.getPropertyH(), 4);
        assertEquals(out.getPropertyL(), ASystem.UNKNOWN);
        assertEquals(out.getNetworkId(), UUID.fromString("b74232be-95fa-11e2-881e-c3733469db4c"));
        assertEquals(out.getPropertyAName(), "Battlefield 4");
        assertEquals(out.getPropertyC(), UUID.fromString("999d6b5a-96c9-11e2-8e51-07bc0e9e2a0b"));
    }


}
