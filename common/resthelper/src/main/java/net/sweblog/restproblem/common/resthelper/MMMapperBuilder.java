package net.sweblog.restproblem.common.resthelper;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

public class MMMapperBuilder {

    private ObjectMapper mapper;

    public MMMapperBuilder() {
        mapper = new ObjectMapper();

        mapper.registerModule(new JodaModule());

        mapper.configure(WRITE_DATES_AS_TIMESTAMPS, false);

        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .registerModule(new JodaModule())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true)
                .configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true)
                .configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, true);

        mapper.setVisibilityChecker(mapper.getVisibilityChecker().withCreatorVisibility(NONE));
        mapper.setVisibilityChecker(mapper.getVisibilityChecker().withFieldVisibility(NONE));
        mapper.setVisibilityChecker(mapper.getVisibilityChecker().withGetterVisibility(NONE));
    }

    public  ObjectMapper build()
    {
        return mapper;
    }

    MMMapperBuilder withFeature(MMMapperFeature feature)
    {
        feature.configureMapper(mapper);

        return this;
    }
}
