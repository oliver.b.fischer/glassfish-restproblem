package net.sweblog.restproblem.common.resthelper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public enum MMMapperFeature {
    INDENT_ON(SerializationFeature.INDENT_OUTPUT, true),
    INDENT_OFF(SerializationFeature.INDENT_OUTPUT, false);

    private final boolean featureState;
    private final SerializationFeature configFeature;

    MMMapperFeature(SerializationFeature feature, boolean state) {
        configFeature = feature;
        featureState = state;
    }

    public SerializationFeature getConfigFeature() {
        return configFeature;
    }

    public boolean isActive() {
        return featureState;
    }

    public void configureMapper(ObjectMapper mapper) {
        mapper.configure(getConfigFeature(), isActive());
    }
}
